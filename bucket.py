# coding:utf-8
#Created by felipe on 16/04/15 - 15:16.
__author__ = 'felipe'

import os

class Bucket:
	def __init__(self,nome):
		self.nome=str(nome)
		diretorio="IO"
		arquivo="bucket_"+self.nome+".txt"
		self.path=diretorio+'/'+arquivo
		if arquivo in os.listdir(diretorio):
			self.arq=open(self.path)
		else:
			self.arq=open(self.path,'w')#write mode
		self.arq.close()
		self.profundidade=None

	def abreLeitura(self):
		self.arq.close()
		self.arq=open(self.path)#read mode

	def abreEscrita(self):
		self.arq.close()
		self.arq=open(self.path,'a')#append mode

	def put(self,registro):
		self.abreEscrita()
		print("Escrevendo",registro)
		self.arq.writelines([str(registro['id'])+','+registro['val']+'\n'])

	def get(self):
		regList=[]
		self.abreLeitura()
		texto=self.arq.readlines()
		for i in texto:
			r=dict()
			r['id']=i.split(',')[0]
			r['val']=i.split(',')[1][:-1]
			regList.append(r)
		return regList

	def end(self):
		self.arq.close()

	def remove(self):
		self.arq.close()
		os.remove(self.path)

	def getSize(self):
		size=os.path.getsize(self.path)
		if size >= 2560:
			return True
		else:
			return False

if __name__=="__main__":
	b1 = Bucket(0)
	b2=Bucket(1)
	r1=dict()
	r2=dict()
	r1['id']=10
	r1['val']=bin(10)
	r2['id']=11
	r2['val']=bin(11)
	b1.put(r1)
	b2.put(r2)
	# b2.get()
	print(b2.get())
	# print(b1.get())
	b1.end()
	b2.end()