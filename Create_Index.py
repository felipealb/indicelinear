#coding: utf-8
import os
from indiceLinear.aleatorioNaoRepetido import Aleatorio
from indiceLinear.bucket import Bucket

class CreateIndex:

	profundidadeGlobal = 2
	diretorio = []
	rnd = Aleatorio(10**6,10**8)

	def __init__(self):
		self.createPages(500)
		for i in range(2**self.profundidadeGlobal):
			self.diretorio.append([])
			self.diretorio[i].append(str(bin(i))[2:])

	def createPages(self,qtde_pgs):
		arquivo = open("IO/registros_não_indexados.txt")
		buf = arquivo.readlines()
		arquivo.close()
		arquivo = open("IO/registros_não_indexados.txt","w")
		arquivo.writelines(buf)
		count = (len(buf))
		qtde_pgs += len(buf)
		while count != qtde_pgs:
			pg_id = count
			idade_bin = bin(self.rnd.gera())

			arquivo.writelines(str(pg_id) + "," + str(idade_bin) + '\n')
			registro = (str(pg_id) + "," + str(idade_bin) + '\n')

			self.indexarRegistro(registro)
			count += 1
		arquivo.close()

	def menu(self):
		print("O que você deseja fazer?")
		print("1 - Inserir um novo registro")
		print("2 - Remover um registro")
		print("3 - Encontrar um ou mais registros de idade")
		print("4 - Scan completo dos buckets (mostrar os buckets)")
		print("5 - Sair")

		menu = int (input())

		if menu == 1:
			self.createPages(1)
			print("Foi inserido mais um arquivo!")
			self.menu()

		elif menu == 2:
			print("Qual o registro que você deseja remover?")
			valor = int(input())
			self.removerFile(valor)

		elif menu == 3:
			print("Qual o registro que você deseja encontrar?")
			valor = int(input())
			self.procurarFile(valor)

		elif menu == 4:
			pass

	def procurarFile(self,pg_id):
		pass

	def removerFile(self,valor):
		indicator=bin(valor)[2:][-self.profundidadeGlobal:]
		if "IO/bucket "+indicator+".txt" in os.listdir():
			arquivo = open("IO/bucket " + indicator + ".txt", "r")
			while True:
				x = arquivo.readline()
				aux1 = x.split(",0b")
				teste = aux1[1]
				if bin(valor)[:2] == teste:
					print("Tem certeza que deseja remover o arquivo " + aux1[0] + aux1[1] + "?" + " Digite 1 para sim e 2 para não.")
					decision = int(input())
					if decision == 1:
						arquivo.close()
						arquivo = open("IO/bucket " + indicator + ".txt", "w")
						for line in arquivo.readline():
							if line!= aux1[0] + ",0b"+ aux1[1] +"\n":
								arquivo.write(line)
					else:
						self.menu()
				else:
					print("Não existe este valor no index!")
					self.menu()

	def lerArquivo(self):
		arquivo = open("IO/registros_indexados.txt")
		print(arquivo.readlines())
		arquivo.close()

	def indexarRegistro(self,registro):
		""" transformando o registro em um split com um pg id e um valor de registro """
		out = registro.split(",0b")
		valor=out[1]
		aux=int(valor[:-1],base=2)
		indicator=bin(aux)[2:][-self.profundidadeGlobal:]

		if "IO/bucket "+ indicator + ".txt" in os.listdir():
			arquivo = open("IO/bucket " + indicator + ".txt")
			buf = arquivo.readlines()
			arquivo.close()
			arquivo = open("IO/bucket " + indicator + ".txt","w")
			arquivo.writelines(buf)
			arquivo.writelines([registro])
			arquivo.close()
		else:
			arquivo = open("IO/bucket " + indicator + ".txt","w")
			arquivo.writelines([registro])
			arquivo.close()

		size = os.path.getsize("IO/bucket " + indicator + ".txt")
		if self.verificaTamanhoBucket(size):
			self.profundidadeGlobal += 1
			self.reordenaBucket()

	def reordenaBucket(self):
		bucket = 0
		while bucket <= self.profundidadeGlobal**2:
			if bin(bucket)[:1] == 0:
				#cria os arquivos e renomeia o bucket antigo do 0
				os.rename("IO/bucket 0" + bin(bucket)[2:] + ".txt","IO/bucket 00" + bin(bucket)[2:] + ".txt")
				arquivo = open("IO/bucket 10" + bin(bucket)[2:] + ".txt","w")
				arquivo.close()

				arquivo = open("IO/bucket 00" + bin(bucket)[2:] + ".txt")
				self.obterRegistro(arquivo)
				arquivo.close()

				arquivo = open("IO/bucket 10" + bin(bucket)[2:] + ".txt")
				self.obterRegistro(arquivo)
				arquivo.close()

			elif bin(bucket)[:1] == 1:
				#cria os buckets e renomeia o antigo do 1
				os.rename("IO/bucket 0" + bin(bucket)[2:] + ".txt" ,"IO/bucket 00" + bin(bucket)[2:] + ".txt")
				arquivo = open("IO/bucket 10" + bin(bucket)[2:] + ".txt","w")
				arquivo.close()

				arquivo = open("IO/bucket 00" + bin(bucket)[2:] + ".txt")
				self.obterRegistro(arquivo)
				arquivo.close()

				arquivo = open("IO/bucket 10" + bin(bucket)[2:] + ".txt")
				self.obterRegistro(arquivo)
				arquivo.close()

			bucket += 1

	def obterRegistro (self,arquivo):
		while True:
			x = arquivo.readline()
			if x == '':
				break
			else: self.indexarRegistro(x)

	def verificaTamanhoBucket(self,size):
		if size >= 2560:
			return True
		else: return False

	def getIndex(self,nome):
		for i in range(self.profundidadeGlobal**2):
			print("----------->",self.diretorio[i][0])
			if self.diretorio[i][0] == nome:
				print("achou!")
				return i

class Indice:
	def __init__(self):
		self.rnd=Aleatorio(100,1000)
		self.profundidade=1
		self.buckets=dict()

	def leBuckets(self):
		#@todo adicionar profundidade em cada bucket
		if os.listdir("IO")==[]:
			print("Não há buckets, criando alguns...")
			for i in range(2**self.profundidade):
				nome=bin(i)[2:]
				self.buckets[nome]=Bucket(nome)
				print("criando",nome)
		else:
			for nome in os.listdir("IO"):
				self.buckets[nome[-5:-4]]=Bucket(nome[-5:-4])#pega apenas os numeros no nome do arquivo
				print("importando",nome[-5:-4])

	def fechaBuckets(self):
		for i in self.buckets.items():
			i[1].end()

	def divideBucket(self,bucket):
		print("Dividindo Bucket",bucket)
		registros=self.buckets[bucket].get()
		b0=Bucket('0'+bucket)
		b1=Bucket('1'+bucket)
		for r in registros:
			if r['val'][2:][-(self.profundidade+1):][0]=='0': #não tente entender isso
				b0.put(r)
			else:
				b1.put(r)
		self.buckets[bucket].remove()


	def indexa(self,registro):
		print(registro['val'])
		pos_bucket=registro['val'][2:][-self.profundidade:]
		self.buckets[pos_bucket].put(registro)

	def gerador(self):
		for i in range(10):
			rid=self.rnd.gera()
			self.indexa(self.criaRegistro(rid,bin(rid)))

	def criaRegistro(self,rid,valor):
		"""
		Registro é um dicionário do tipo R[chave]=valor
		"""
		registro=dict()
		registro["id"]=rid
		registro["val"]=valor
		return registro

if __name__=="__main__":
	i=Indice()
	i.leBuckets()
	i.gerador()
	print(i.buckets)
	i.divideBucket('0')
	i.divideBucket('11')
	i.fechaBuckets()