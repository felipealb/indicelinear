# coding:utf-8
#Created by tioago on 15/04/15 - 21:54.
__author__ = 'tioago'

import random

class Aleatorio:
	""" Gera um número aleatório não repetido"""

	def __init__(self,inicio,fim):
		self.numeros=[]
		self.inicio=inicio
		self.fim=fim

	def gera(self):
		if len(self.numeros)==self.fim-self.inicio:
			return None
			#Se a quantidade máxima de elementos não repetidos for
			#atingida, retorna None
		while True:
			n=random.randint(self.inicio,self.fim)
			if n not in self.numeros:
				self.numeros.append(n)
				return n
			else:
				None

if __name__=="__main__":
	a=Aleatorio(1,10)
	for i in range(20):
		print(a.gera())